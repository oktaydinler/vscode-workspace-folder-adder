'use strict';
// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from 'vscode';
import { lstatSync } from 'fs';
import { parse } from 'path';
import { EOL } from 'os';

// this method is called when your extension is activated
// your extension is activated the very first time the command is executed
export function activate(context: vscode.ExtensionContext) {
    // Use the console to output diagnostic information (console.log) and errors (console.error)
    // This line of code will only be executed once when your extension is activated
    console.log('Congratulations, your extension "test" is now active!');

    // The c    ommand has been defined in the package.json file
    // Now provide the implementation of the command with  registerCommand
    // The commandId parameter must match the command field in package.json
    context.subscriptions.push(vscode.commands.registerCommand('extension.addFolderToWorkspace', file => {
        // The code you place here will be executed every time your command is executed
        if (!file
            || !file.fsPath
            || !vscode.workspace.workspaceFolders
            || !lstatSync(file.fsPath).isDirectory()
            || !vscode.workspace.rootPath
        ) {
            return;
        }

        const rootPathDirName = parse(vscode.workspace.rootPath).name;

        vscode.workspace.updateWorkspaceFolders(
            vscode.workspace.workspaceFolders.length,
            undefined,
            {
                name: file.fsPath.substr(file.fsPath.indexOf(rootPathDirName)),
                uri: vscode.Uri.file(file.fsPath)
            }
        );
    }));
    context.subscriptions.push(vscode.commands.registerCommand('extension.xmlNewLine', e => {
        const editor = vscode.window.activeTextEditor;

        // const whitelist: Array<String> = ['xml', 'html', 'php', 'svg'];

        if (!editor
            || !editor.selection.isEmpty
            // || !whitelist.includes(editor.document.languageId)
        ) {
            return;
        }

        const selection = editor.selection;
        const pos = new vscode.Position(
            selection.start.line,
            selection.start.character
        );

        // const x = editor.document.getWordRangeAtPosition(
        //     pos,
        //     /\ [a-z].+?\=\".*?\"/
        // );
        // if (!x) {
        //     return;
        // }

        const textLine = editor.document.lineAt(pos.line);
        const firstAttrAt = textLine.text.search(/\ [a-z].+?\=\".*?\"/);

        if (firstAttrAt === -1) {
            return;
        }

        // const firstAttr = new vscode.Position(x.start.line, x.start.character + 1);

        editor.edit(function (builder) {
            // builder.insert(pos, '\n' + ' '.repeat(x.start.character + 1));
            builder.insert(pos, EOL + ' '.repeat(firstAttrAt + 1));
        });

    }));
}

// this method is called when your extension is deactivated
export function deactivate() {
}
