## Features

- Right click a directory to quickly add the folder to your workspace.
- Press `ctrl+shift+alt+n` in a markup language to 'pretty' a newline
  - example:
    ```xml
    <person name="John Doe" age="18" />
    <!-- becomes -->
    <person name="John Doe"
            age="18" />
    ```

<details>
  <summary>Click to expand</summary>

  ![pepe gif](https://bitbucket.org/oktaydinler/vscode-workspace-folder-adder/raw/b9e68ab0b6296df5ff813f5f699d43aaf22a6406/pepe.gif)

</details>


