# Change Log

## 0.0.4
### Added
- `ctrl+shift+alt+n` to pretty-newline for markup languages

## 0.0.1
- Initial release
